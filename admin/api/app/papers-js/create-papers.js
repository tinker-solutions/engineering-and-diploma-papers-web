$(document).ready(function() {

    // show html form when 'create product' button was clicked
    var fileurl = "";
    $(document).on('click', '.create-product-button', function() {

        // get product id
        var id = $(this).attr('data-id');



        // read one record based on given product id
        $.getJSON("http://localhost/muapp/admin/api/papers/read_one.php?id=" + id, function(data) {

            // values will be used to fill out our form
            var id = data.id;
            var subject = data.subject;
            var stream = data.stream;
            var semester = data.semester;
            var formno = 1;

            // Months
            $.getJSON("http://localhost/muapp/admin/api/category/month-read.php", function(data) {
                // build categories option html
                // loop through returned list of data
                var categories_options_html_month = "";
                categories_options_html_month += "<select name='category_month_id' class='form-control'>";
                $.each(data.records, function(key, val) {
                    categories_options_html_month += "<option value='" + val.id + "'>" + val.name + "</option>";
                });
                categories_options_html_month += "</select>";

                //Years
                $.getJSON("http://localhost/muapp/admin/api/category/year-read.php", function(data) {
                    // build categories option html
                    // loop through returned list of data
                    var categories_options_html_year = "";
                    categories_options_html_year += "<select name='category_year_id' class='form-control'>";
                    $.each(data.records, function(key, val) {
                        categories_options_html_year += "<option value='" + val.id + "'>" + val.name + "</option>";
                    });
                    categories_options_html_year += "</select>";


                    //Syllabus
                    $.getJSON("http://localhost/muapp/admin/api/category/syllabus-read.php", function(data) {
                        // build categories option html
                        // loop through returned list of data
                        var categories_options_html_syllabus = "";
                        categories_options_html_syllabus += "<select name='category_syllabus_id' class='form-control'>";
                        $.each(data.records, function(key, val) {
                            categories_options_html_syllabus += "<option value='" + val.id + "'>" + val.name + "</option>";
                        });
                        categories_options_html_syllabus += "</select>";




                        // we have our html form here where product information will be entered
                        // we used the 'required' html5 property to prevent empty fields
                        var create_papers_html = "";
                        create_papers_html += "<div class='btn-group-vertical pull-right m-b-50px'>";
                        // 'read products' button to show list of products
                        create_papers_html += "<div id='read-products' class='btn btn-primary pull-right m-b-15px read-products-button'>";
                        create_papers_html += "<span class='glyphicon glyphicon-list'></span> All Papers";
                        create_papers_html += "</div>";


                        // 'Add Fields' button to add fields
                        // create_papers_html+="<div id='add-fields' class='btn btn-primary pull-right m-b-15px add-fields'>";
                        // create_papers_html+="<span class='glyphicon glyphicon-plus'></span> Add Fields";
                        // create_papers_html+="</div>";

                        create_papers_html += "</div>";

                        // 'create product' html form
                        create_papers_html += "<h2>FORM NO : " + formno + " </h2>";
                        create_papers_html += "<div id='insert-form'>";
                        create_papers_html += "<form id='create-product-form' action='#' method='post' border='0'>";
                        create_papers_html += "<table class='table table-hover table-responsive table-bordered'>";

                        // ID  field
                        create_papers_html += "<tr>";
                        create_papers_html += "<td>ID</td>";
                        create_papers_html += "<td><input type='text' value=\"" + id + "\" name='subject_stream_semester_id' class='form-control' required /></td>";
                        create_papers_html += "</tr>";

                        // Subject name field
                        create_papers_html += "<tr>";
                        create_papers_html += "<td>Subject name</td>";
                        create_papers_html += "<td><input type='text' value=\"" + subject + "\" name='subject_stream_semester_name' class='form-control' required /></td>";
                        create_papers_html += "</tr>";

                        // Stream field
                        create_papers_html += "<tr>";
                        create_papers_html += "<td>Stream</td>";
                        create_papers_html += "<td><input type='text' value=\"" + stream + "\"  name='subject_stream_semester_stream' class='form-control' required /></td>";
                        create_papers_html += "</tr>";

                        // Semester field
                        create_papers_html += "<tr>";
                        create_papers_html += "<td>Semester</td>";
                        create_papers_html += "<td><input type='text' value=\"" + semester + "\"  name='subject_stream_semester_semester' class='form-control' required /></td>";
                        create_papers_html += "</tr>";

                        // categories 'select Month' field
                        create_papers_html += "<tr>";
                        create_papers_html += "<td>Month</td>";
                        create_papers_html += "<td>" + categories_options_html_month + "</td>";
                        create_papers_html += "</tr>";

                        // categories 'select Year' field
                        create_papers_html += "<tr>";
                        create_papers_html += "<td>Year</td>";
                        create_papers_html += "<td>" + categories_options_html_year + "</td>";
                        create_papers_html += "</tr>";

                        // categories 'select Syllabus ' field

                        create_papers_html += "<tr>";
                        create_papers_html += "<td>Syllabus</td>";
                        create_papers_html += "<td>" + categories_options_html_syllabus + "</td>";
                        create_papers_html += "</tr>";



                        // categories 'select' field
                        create_papers_html += "<tr>";
                        create_papers_html += "<td>Upload Paper PDF : <input type='text' id='file-name' name='fileurl' class='form-control'/></td>";
                        // create_papers_html+="<td><input id='fileupload' type='file' name='files[]' data-url='server/php/' multiple> <button id='confirm-upload' type='button' class='btn btn-success m-10-all'>Upload</button>   </td>";
                        create_papers_html += "<td><span class='btn-lg btn-danger m-t-10 fileinput-button'>  <span>Choose Pdf</span>  <input id='fileupload' type='file'  name='files[]'  data-url='api/server/" + stream + "/" + semester + "/' multiple accept='application/pdf'> </span>  <button id='confirm-upload' type='button' class='btn-lg btn-success btn-up text-left  m-t-10px fileinput-button'> Upload</button></td>";
                        create_papers_html += "</tr>";



                        // button to submit form
                        create_papers_html += "<tr>";
                        create_papers_html += "<td></td>";
                        create_papers_html += "<td>";
                        create_papers_html += "<button type='submit' id='btnSubmit' class='btn btn-primary'>";
                        create_papers_html += "<span class='glyphicon glyphicon-ok'></span> Add Paper";
                        create_papers_html += "</button>";
                        create_papers_html += "</td>";
                        create_papers_html += "</tr>";

                        create_papers_html += "</table>";
                        create_papers_html += "</form>";
                        create_papers_html += "</div>";

                        // inject html to 'page-content' of our app
                        $("#page-content").html(create_papers_html);

                        // chage page title
                        changePageTitle("Insert Papers");

                        // //append form
                        // $(document).on('click', '#add-fields', function(){   
                        //     formno++;
                        //     var formvar="<div id='insert-form'> <form id='create-product-form' action='#' method='post' border='0'> <table class='table table-hover table-responsive table-bordered'> <tr><td>ID</td> <td><input type='text' value=\"" + id + "\" name='subject_stream_semester_id' class='form-control' required /></td></tr> <tr><td>Subject name</td> <td><input type='text' value=\"" + subject + "\" name='subjectname' class='form-control' required /></td> </tr> <tr> <td>Stream</td> <td><input type='text' value=\"" + stream + "\" name='streamname' class='form-control' required /></td> </tr> <tr> <td>Semester</td> <td><input type='text' value=\"" + semester + "\" name='semesterame' class='form-control' required /></td> </tr> <tr> <td>Month</td> <td>" + categories_options_html_month + "</td> </tr> <tr> <td>Year</td> <td>" + categories_options_html_year + "</td> </tr> <tr> <td>Syllabus</td> <td>" + categories_options_html_syllabus + "</td> </tr> <td>Upload Paper PDF : <p id='file-name'></p></td> <td><span class='btn-lg btn-danger m-t-10 fileinput-button'> <span>Choose Pdf</span> <input id='fileupload' type='file' name='files[]' data-url='server/" + stream + "/" + semester + "/' multiple> </span> <button id='confirm-upload' type='button' class='btn-lg btn-success btn-up text-left m-t-10px fileinput-button'> Upload</button></td> </tr> <tr> <td></td> <td> <button type='submit'id='btnSubmit' class='btn btn-primary'> <span class='glyphicon glyphicon-ok'></span> Create Product </button> </td> </tr> </table> </form> </div>";

                        //     $("#insert-form").append("<h2>FORM NO : " + formno+ " </h2> "+ formvar +"");

                        //     });


                        //fileupload methods

                        $(document).on('click', '#fileupload', function() {
                            $(function() {
                                $('#fileupload').fileupload({
                                    dataType: 'json',
                                    add: function(e, data) {
                                        data.context = $('#confirm-upload')
                                            .click(function() {
                                                data.context = $('<p/>').text('Uploading...').replaceAll($(this));
                                                data.submit();

                                            });
                                    },
                                    done: function(e, data) {


                                        fileurl = 'http://muapp.tinkersolutions.co.in/admin/api/server/' + stream + '/' + semester + '/' +"files/"+ data.files[0].name;
                                        data.context.text('File Upload finished.');

                                        document.getElementById("file-name").value = fileurl;
                                    },
                                    fail: function(e, data) {


                                    }
                                });
                            });
                        });



                    });
                });
            });
        });
    });
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    // will run if create product form was submitted
    $(document).on('submit', '#create-product-form', function() {
       
       // get form data
var form_data=JSON.stringify($(this).serializeObject());
// submit form data to api
$.ajax({
    url: "http://localhost/muapp/admin/api/papers/create.php",
    type : "POST",
    contentType : 'application/json',
    data : form_data,
    success : function(result) {
        // product was created, go back to products list
        showProductsFirstPage();
            },
    error: function(xhr, resp, text) {
        // show error to console
        console.log(xhr, resp, text);
    }
});
        return false;
    });




});