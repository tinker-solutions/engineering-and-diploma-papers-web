// product list html
function readProductsTemplate(data, keywords) {

    var read_papers_html = "";

    // search products form
    read_papers_html += "<form id='update-search-product-form' action='#' method='post'>";
    read_papers_html += "<div class='input-group pull-left w-30-pct '>";

    read_papers_html += "<input type='text' value=\"" + keywords + "\" name='keywords' class='form-control product-search-keywords' placeholder='Search papers...' />";

    read_papers_html += "<span class='input-group-btn'>";
    read_papers_html += "<button type='submit' class='btn btn-default' type='button'>";
    read_papers_html += "<span class='glyphicon glyphicon-search'></span>";
    read_papers_html += "</button>";
    read_papers_html += "</span>";
    read_papers_html += "</div><br><br><br>";

    read_papers_html += "</form>";



    // start table

    read_papers_html += "<table class='table table-bordered table-hover'>";

    // creating our table heading
    read_papers_html += "<tr>";
    read_papers_html += "<th class='w-25-pct'>Subject</th>";
    read_papers_html += "<th class='w-10-pct'>Stream</th>";
    read_papers_html += "<th class='w-15-pct'>Semester</th>";
    read_papers_html += "<th class='w-25-pct text-align-center'>Action</th>";
    read_papers_html += "</tr>";

    // loop through returned list of data
    $.each(data.records, function(key, val) {

        // creating new table row per record
        read_papers_html += "<tr>";

        read_papers_html += "<td>" + val.subject + "</td>";
        read_papers_html += "<td>" + val.stream + "</td>";
        read_papers_html += "<td>" + val.semester + "</td>";

        // 'action' buttons
        read_papers_html += "<td>";



        // when clicked, it will load the create product form
        read_papers_html += "<div id='create-product' class='btn btn-primary  m-l-50px create-product-button' data-id='" + val.id + "'>";
        read_papers_html += "<span class='glyphicon glyphicon-plus'></span> Create ";
        read_papers_html += "</div>";


        read_papers_html += "</td>";

        read_papers_html += "</tr>";

    });

    // end table
    read_papers_html += "</table>";

    // pagination
if(data.paging){
    read_papers_html+="<ul class='pagination pull-left margin-zero padding-bottom-2em'>";
 
        // first page
        if(data.paging.first!=""){
            read_papers_html+="<li><a data-page='" + data.paging.first + "'>First Page</a></li>";
        }
 
        // loop through pages
        $.each(data.paging.pages, function(key, val){
            var active_page=val.current_page=="yes" ? "class='active'" : "";
            read_papers_html+="<li " + active_page + "><a data-page='" + val.url + "'>" + val.page + "</a></li>";
        });
 
        // last page
        if(data.paging.last!=""){
            read_papers_html+="<li><a data-page='" + data.paging.last + "'>Last Page</a></li>";
        }
        read_papers_html+="</ul>";
}

    // inject to 'page-content' of our app
    $("#page-content").html(read_papers_html);
}