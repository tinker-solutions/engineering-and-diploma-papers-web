$(document).ready(function(){
 
    // show list of product on first load
    showProducts();
    // when a 'read products' button was clicked
$(document).on('click', '.read-products-button', function(){
    showProducts();
});
 
});

// function to show list of products
/*function showProducts(){
    // get list of products from the API
$.getJSON("http://localhost/muapp/admin/api/papers/read.php", function(data){
    // html for listing products
var read_products_html="";
 

// start table
read_products_html+="<table class='table table-bordered table-hover'>";
 
    // creating our table heading
    read_products_html+="<tr>";
        read_products_html+="<th class='w-25-pct'>Subject</th>";
        read_products_html+="<th class='w-10-pct'>Stream</th>";
        read_products_html+="<th class='w-15-pct'>Semester</th>";
        read_products_html+="<th class='w-25-pct text-align-center'>Action</th>";
    read_products_html+="</tr>";
     
    // loop through returned list of data
$.each(data.records, function(key, val) {
 
    // creating new table row per record
    read_products_html+="<tr>";
 
        read_products_html+="<td>" + val.subject + "</td>";
        read_products_html+="<td>" + val.stream + "</td>";
        read_products_html+="<td>" + val.semester + "</td>";
 
        // 'action' buttons
        read_products_html+="<td>";
       


            // when clicked, it will load the create product form
read_products_html+="<div id='create-product' class='btn btn-primary  m-l-30px create-product-button' data-id='" + val.id + "'>";
read_products_html+="<span class='glyphicon glyphicon-plus'></span> Create ";
read_products_html+="</div>";


        read_products_html+="</td>";
 
    read_products_html+="</tr>";
 
});
 
// end table
read_products_html+="</table>";
// inject to 'page-content' of our app
$("#page-content").html(read_products_html);
// chage page title
changePageTitle("Subjects");
 
});
 
}*/
// function to show list of products
function showProducts(){
 
    // get list of products from the API
    $.getJSON("http://localhost/muapp/admin/api/papers/read.php", function(data){
 
        // html for listing products
        readProductsTemplate(data, "");
 
        // chage page title
        changePageTitle("Engineering Papers List");
 
    });
}