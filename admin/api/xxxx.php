<!DOCTYPE html>
<html lang="en">
<head>
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <title>Read Products</title>
 
    <!-- bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
 
    <!-- custom CSS -->
    <link href="app/assets/css/style.css" rel="stylesheet" />
    <!-- Generic page styles -->
<link rel="stylesheet" href="app/assets/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="app/assets/css/jquery.fileupload.css">
 
</head>
<body>
 
<!-- our app will be injected here -->
<div id="app"></div>
 
<!-- jQuery library -->
<script src="app/assets/js/jquery.js"></script>
 
<!-- bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
<!-- bootbox for confirm pop up -->
<script src="app/assets/js/bootbox.min.js"></script>
 
<!-- app js script -->
<script src="app/app.js"></script>
 
<!-- products scripts -->
<script src="app/papers-js/read-papers.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="js/vendor/jquery.ui.widget.js"></script>
<script src="js/jquery.iframe-transport.js"></script>
<script src="js/jquery.fileupload.js"></script>
<script src="app/papers-js/create-papers.js"></script>

 
</body>
</html>