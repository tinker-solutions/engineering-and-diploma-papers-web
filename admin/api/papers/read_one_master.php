<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../config/database.php';
include_once '../objects/paper-objects.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare PaperObjects object
$product = new PaperObjects($db);

// set ID property of PaperObjects to be edited
$product->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of product to be edited
$product->readOneMaster();

// create array
$product_arr = array(
    "Id" => $product->id,
    "Subject" => $product->subject,
    "Semester" => $product->semester,
    "Stream" => $product->stream,
    "Syllabus" => $product->syllabus,
    "Paper" => $product->paper

);

// make it json format
print_r(json_encode($product_arr));
