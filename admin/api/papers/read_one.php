<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../config/database.php';
include_once '../objects/paper-objects.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare PaperObjects object
$product = new PaperObjects($db);

// set ID property of PaperObjects to be edited
$product->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of product to be edited
$product->readOne();

// create array
$product_arr = array(
    "id" => $product->id,
    "subject" => $product->subject,
    "semester" => $product->semester,
    "stream" => $product->stream

);

// make it json format
print_r(json_encode($product_arr));
