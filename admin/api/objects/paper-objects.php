<?php
class PaperObjects
{

    // database connection and table name
    private $conn;
    private $table_name_subject_stream_semester = "subject_stream_semester";
    private $table_name_master = "master";
    private $table_name="subject_stream_semesterr";

    // object properties
    public $id;
    public $subject;
    public $semester;
    public $stream;
    public $month;
    public $year;
    public $syllabus;
    public $papers;
   

    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

// read products
    public function read()
    {

        // select all query
        $query = "SELECT subject_stream_semester.id, subject.subject_name, semester.semester_name, stream.stream_name FROM
        ((( " . $this->table_name_subject_stream_semester . " INNER JOIN subject ON subject_stream_semester.subject_id = subject.subject_id) INNER JOIN semester ON subject_stream_semester.semester_id = semester.semester_id) INNER JOIN stream ON subject_stream_semester.stream_id = stream.stream_id)";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }
    // read products
    public function readMaster()
    {

        // select all query
        $query = "select master.id,stream.stream_name, semester.semester_name, subject.subject_name,master.link_paper,master.link_syllabus from master left join subject on master.subject_stream_semester_id = subject.subject_id left join stream on subject.subject_id = stream.stream_id left join semester on subject.subject_id = semester.semester_id LEFT JOIN syllabus on master.link_paper=syllabus.syllabus_link LEFT JOIN syllabus s on master.link_syllabus=syllabus.syllabus_link";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

     // read products
     public function readOneMaster()
     {
 
         // select all query
         $query = "select master.id,stream.stream_name, 
         semester.semester_name, 
         subject.subject_name,
         master.link_paper,
         master.link_syllabus 
         from master 
         left join subject on master.subject_stream_semester_id = subject.subject_id 
         left join stream on subject.subject_id = stream.stream_id 
         left join semester on subject.subject_id = semester.semester_id 
         LEFT JOIN syllabus on master.link_paper=syllabus.syllabus_link 
         LEFT JOIN syllabus s on master.link_syllabus=syllabus.syllabus_link
         WHERE
         master.id = ?
         LIMIT
         0,1";
 
       // prepare query statement
       $stmt = $this->conn->prepare($query);

       // bind id of product to be updated
       $stmt->bindParam(1, $this->id);

       // execute query
       $stmt->execute();

       // get retrieved row
       $row = $stmt->fetch(PDO::FETCH_ASSOC);

       // set values to object properties
       $this->id = $row['id'];
       $this->stream = $row['stream_name'];
       $this->subject = $row['subject_name'];
       $this->semester = $row['semester_name'];
       $this->paper = $row['link_paper'];
       $this->syllabus = $row['link_syllabus'];
     }

// create product
function create(){
 
    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name_master. "
            SET
                subject_stream_semester_id=:id,
                month_id=:month, year_id=:year,
                link_paper=:papers
                link_syllabus=:syllabus";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->id=htmlspecialchars(strip_tags($this->id));
    $this->month=htmlspecialchars(strip_tags($this->month));
    $this->year=htmlspecialchars(strip_tags($this->year));
    $this->papers=htmlspecialchars(strip_tags($this->papers));
    $this->syllabus=htmlspecialchars(strip_tags($this->syllabus));
 
    // bind values
    $stmt->bindParam(":id", $this->id);
    $stmt->bindParam(":month", $this->month);
    $stmt->bindParam(":year", $this->year);
    $stmt->bindParam(":papers", $this->papers);
    $stmt->bindParam(":syllabus", $this->syllabus);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}
// update the product
public function update()
{

        // update query
        $query = " UPDATE
        
                " . $this->table_name_master. "
            
        SET
        subject_stream_semester_id=:id,
        month_id=:month, year_id=:year,
        link_paper=:papers
        link_syllabus=:syllabus";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

       // sanitize
    $this->id=htmlspecialchars(strip_tags($this->id));
    $this->month=htmlspecialchars(strip_tags($this->month));
    $this->year=htmlspecialchars(strip_tags($this->year));
    $this->papers=htmlspecialchars(strip_tags($this->papers));
    $this->syllabus=htmlspecialchars(strip_tags($this->syllabus));
 
    // bind values
    $stmt->bindParam(":id", $this->id);
    $stmt->bindParam(":month", $this->month);
    $stmt->bindParam(":year", $this->year);
    $stmt->bindParam(":papers", $this->papers);
    $stmt->bindParam(":syllabus", $this->syllabus);

        // execute the query
        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

// delete the product
    public function delete()
    {

        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->id = htmlspecialchars(strip_tags($this->id));

        // bind id of record to delete
        $stmt->bindParam(1, $this->id);

        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;

    }

// used when filling up the update product form
    public function readOne()
    {

        // query to read single record
        $query = "SELECT subject_stream_semester.id, subject.subject_name, semester.semester_name, stream.stream_name FROM
            ((( " . $this->table_name_subject_stream_semester . " INNER JOIN subject ON subject_stream_semester.subject_id = subject.subject_id)
            INNER JOIN semester ON subject_stream_semester.semester_id = semester.semester_id)
            INNER JOIN stream ON subject_stream_semester.stream_id = stream.stream_id)
                    WHERE
                        subject_stream_semester.id = ?
                    LIMIT
                        0,1";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind id of product to be updated
        $stmt->bindParam(1, $this->id);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->subject = $row['subject_name'];
        $this->semester = $row['semester_name'];
        $this->stream = $row['stream_name'];

    }

    // search products
    public function search($keywords)
    {

        // select all query
        $query = "SELECT a.id, b.subject_name, c.semester_name, d.stream_name FROM
        ((( subject_stream_semester a LEFT JOIN subject b ON a.subject_id = b.subject_id)
        LEFT JOIN semester c ON a.semester_id = c.semester_id)
        LEFT JOIN stream d ON a.stream_id = d.stream_id)
        WHERE
        b.subject_name LIKE ? OR c.semester_name LIKE ? OR d.stream_name LIKE ?
            ORDER BY
            a.id ASC";
      
      
   

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // sanitize
        $keywords = htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";

        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);

        // execute query
        $stmt->execute();

        return $stmt;
    }

     // search products
     public function Updatesearch($keywords)
     {
 
         // select all query
         $query = "select master.id,stream.stream_name, semester.semester_name, subject.subject_name,
          master.link_paper,master.link_syllabus from master
          left join subject on master.subject_stream_semester_id = subject.subject_id 
          left join stream on subject.subject_id = stream.stream_id 
          left join semester on subject.subject_id = semester.semester_id 
          LEFT JOIN syllabus on master.link_paper=syllabus.syllabus_link 
          LEFT JOIN syllabus s on master.link_syllabus=syllabus.syllabus_link
         WHERE
         subject.subject_name LIKE ? OR semester.semester_name LIKE ? OR stream.stream_name LIKE ?
             ORDER BY
             master.id ASC";
       
       
    
 
         // prepare query statement
         $stmt = $this->conn->prepare($query);
 
         // sanitize
         $keywords = htmlspecialchars(strip_tags($keywords));
         $keywords = "%{$keywords}%";
 
         // bind
         $stmt->bindParam(1, $keywords);
         $stmt->bindParam(2, $keywords);
         $stmt->bindParam(3, $keywords);
 
         // execute query
         $stmt->execute();
 
         return $stmt;
     }

// read products with pagination
    public function readPaging($from_record_num, $records_per_page)
    {

        // select query
        $query = "SELECT subject_stream_semester.id, subject.subject_name, semester.semester_name, stream.stream_name FROM
        ((( " . $this->table_name_subject_stream_semester . " INNER JOIN subject ON subject_stream_semester.subject_id = subject.subject_id) INNER JOIN semester ON subject_stream_semester.semester_id = semester.semester_id) INNER JOIN stream ON subject_stream_semester.stream_id = stream.stream_id)
            ORDER BY subject_stream_semester.id ASC
            LIMIT ?, ?";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

        // execute query
        $stmt->execute();

        // return values from database
        return $stmt;
    }
// used for paging products
    public function count()
    {
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name_subject_stream_semester . "";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['total_rows'];
    }

    public function readPagingUpdate($from_record_num, $records_per_page)
    {

        // select query
        $query = "select master.id,stream.stream_name, semester.semester_name, subject.subject_name,master.link_paper,master.link_syllabus from master left join subject on master.subject_stream_semester_id = subject.subject_id left join stream on subject.subject_id = stream.stream_id left join semester on subject.subject_id = semester.semester_id LEFT JOIN syllabus on master.link_paper=syllabus.syllabus_link LEFT JOIN syllabus s on master.link_syllabus=syllabus.syllabus_link
            ORDER BY master.id ASC
            LIMIT ?, ?";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

        // execute query
        $stmt->execute();

        // return values from database
        return $stmt;
    }
// used for paging products
    public function countUpdate()
    {
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name_master . "";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['total_rows'];
    }

}
