<?php
class Category
{

    // database connection and table name
    private $conn;
    private $table_name_month = "month";
    private $table_name_year = "year        ";
    private $table_name_syllabus = "syllabus";

    // object properties
    public $month_id;
    public $month_name;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    // used by select drop-down list
    public function MonthreadAll()
    {
        //select all data
        $query = "SELECT
                    month_id, month_name
                FROM
                    " . $this->table_name_month . "
                ORDER BY
                month_id";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    // used by select drop-down list
    public function Monthread()
    {

        //select all data
        $query = "SELECT
    month_id, month_name
            FROM
                 " . $this->table_name_month . "
            ORDER BY
            month_id";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }


     // used by select drop-down list
     public function YearreadAll()
     {
         //select all data
         $query = "SELECT
                     year_id, year_name
                 FROM
                     " . $this->table_name_year . "
                 ORDER BY
                 year_id";
 
         $stmt = $this->conn->prepare($query);
         $stmt->execute();
 
         return $stmt;
     }


    // used by select drop-down list
    public function Yearread()
    {

        //select all data
        $query = "SELECT
    year_id, year_name
            FROM
                 " . $this->table_name_year . "
            ORDER BY
            year_id";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

     // used by select drop-down list
     public function SyllabusreadAll()
     {
         //select all data
         $query = "SELECT
                     syllabus_id, syllabus_link
                 FROM
                     " . $this->table_name_syllabus . "
                 ORDER BY
                 syllabus_id";
 
         $stmt = $this->conn->prepare($query);
         $stmt->execute();
 
         return $stmt;
     }


    // used by select drop-down list
    public function Syllabusread()
    {

        //select all data
        $query = "SELECT
    syllabus_id, syllabus_link
            FROM
                 " . $this->table_name_syllabus . "
            ORDER BY
            syllabus_id";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }
}
